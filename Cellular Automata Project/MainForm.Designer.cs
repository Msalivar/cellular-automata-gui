﻿namespace Cellular_Automata_Project
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.oneDimRunButton = new System.Windows.Forms.Button();
            this.gridCheck = new System.Windows.Forms.CheckBox();
            this.resolutionBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.oooBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ooiBox = new System.Windows.Forms.CheckBox();
            this.oioBox = new System.Windows.Forms.CheckBox();
            this.oiiBox = new System.Windows.Forms.CheckBox();
            this.iiiBox = new System.Windows.Forms.CheckBox();
            this.iioBox = new System.Windows.Forms.CheckBox();
            this.ioiBox = new System.Windows.Forms.CheckBox();
            this.iooBox = new System.Windows.Forms.CheckBox();
            this.genTab = new System.Windows.Forms.TabControl();
            this.oneDTab = new System.Windows.Forms.TabPage();
            this.oneDimRandButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lifeTab = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.liveCellSlider = new System.Windows.Forms.TrackBar();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pauseBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.twoDSpeedBar = new System.Windows.Forms.TrackBar();
            this.borderlessBox = new System.Windows.Forms.CheckBox();
            this.simModeBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.birthBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.surviveBox = new System.Windows.Forms.TextBox();
            this.customSelect = new System.Windows.Forms.RadioButton();
            this.assimilationSelect = new System.Windows.Forms.RadioButton();
            this.gameOfLifeSelect = new System.Windows.Forms.RadioButton();
            this.seedsSelect = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            this.twoDimRunButton = new System.Windows.Forms.Button();
            this.TwoDGenTab = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.genLiveCellSlider = new System.Windows.Forms.TrackBar();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.genPauseBox = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.genSpeedBar = new System.Windows.Forms.TrackBar();
            this.genBorderedBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.genCountBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.genBirthBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.genSurviveBox = new System.Windows.Forms.TextBox();
            this.genCustomSelect = new System.Windows.Forms.RadioButton();
            this.brainSelect = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.genStopButton = new System.Windows.Forms.Button();
            this.genRunButton = new System.Windows.Forms.Button();
            this.gridPanel = new Cellular_Automata_Project.CustomPanel();
            ((System.ComponentModel.ISupportInitialize)(this.resolutionBar)).BeginInit();
            this.genTab.SuspendLayout();
            this.oneDTab.SuspendLayout();
            this.lifeTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.liveCellSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoDSpeedBar)).BeginInit();
            this.simModeBox.SuspendLayout();
            this.TwoDGenTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genLiveCellSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.genSpeedBar)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // oneDimRunButton
            // 
            this.oneDimRunButton.Location = new System.Drawing.Point(6, 6);
            this.oneDimRunButton.Name = "oneDimRunButton";
            this.oneDimRunButton.Size = new System.Drawing.Size(295, 29);
            this.oneDimRunButton.TabIndex = 1;
            this.oneDimRunButton.Text = "Run Simulation";
            this.oneDimRunButton.UseVisualStyleBackColor = true;
            this.oneDimRunButton.Click += new System.EventHandler(this.oneDimRunButton_click);
            // 
            // gridCheck
            // 
            this.gridCheck.AutoSize = true;
            this.gridCheck.Checked = true;
            this.gridCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gridCheck.Location = new System.Drawing.Point(631, 12);
            this.gridCheck.Name = "gridCheck";
            this.gridCheck.Size = new System.Drawing.Size(73, 17);
            this.gridCheck.TabIndex = 4;
            this.gridCheck.Text = "Grid Lines";
            this.gridCheck.UseVisualStyleBackColor = true;
            this.gridCheck.CheckedChanged += new System.EventHandler(this.gridCheck_CheckedChanged);
            // 
            // resolutionBar
            // 
            this.resolutionBar.LargeChange = 10;
            this.resolutionBar.Location = new System.Drawing.Point(776, 4);
            this.resolutionBar.Maximum = 60;
            this.resolutionBar.Name = "resolutionBar";
            this.resolutionBar.Size = new System.Drawing.Size(157, 45);
            this.resolutionBar.SmallChange = 5;
            this.resolutionBar.TabIndex = 5;
            this.resolutionBar.TabStop = false;
            this.resolutionBar.ValueChanged += new System.EventHandler(this.resolutionBar_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(710, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Resolution:";
            // 
            // oooBox
            // 
            this.oooBox.AutoSize = true;
            this.oooBox.Location = new System.Drawing.Point(24, 70);
            this.oooBox.Name = "oooBox";
            this.oooBox.Size = new System.Drawing.Size(50, 17);
            this.oooBox.TabIndex = 7;
            this.oooBox.Text = "0 0 0";
            this.oooBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Rule Set (Checked = 1):";
            // 
            // ooiBox
            // 
            this.ooiBox.AutoSize = true;
            this.ooiBox.Checked = true;
            this.ooiBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ooiBox.Location = new System.Drawing.Point(24, 93);
            this.ooiBox.Name = "ooiBox";
            this.ooiBox.Size = new System.Drawing.Size(50, 17);
            this.ooiBox.TabIndex = 9;
            this.ooiBox.Text = "0 0 1";
            this.ooiBox.UseVisualStyleBackColor = true;
            // 
            // oioBox
            // 
            this.oioBox.AutoSize = true;
            this.oioBox.Location = new System.Drawing.Point(80, 70);
            this.oioBox.Name = "oioBox";
            this.oioBox.Size = new System.Drawing.Size(50, 17);
            this.oioBox.TabIndex = 10;
            this.oioBox.Text = "0 1 0";
            this.oioBox.UseVisualStyleBackColor = true;
            // 
            // oiiBox
            // 
            this.oiiBox.AutoSize = true;
            this.oiiBox.Checked = true;
            this.oiiBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.oiiBox.Location = new System.Drawing.Point(80, 93);
            this.oiiBox.Name = "oiiBox";
            this.oiiBox.Size = new System.Drawing.Size(50, 17);
            this.oiiBox.TabIndex = 11;
            this.oiiBox.Text = "0 1 1";
            this.oiiBox.UseVisualStyleBackColor = true;
            // 
            // iiiBox
            // 
            this.iiiBox.AutoSize = true;
            this.iiiBox.Location = new System.Drawing.Point(80, 139);
            this.iiiBox.Name = "iiiBox";
            this.iiiBox.Size = new System.Drawing.Size(50, 17);
            this.iiiBox.TabIndex = 15;
            this.iiiBox.Text = "1 1 1";
            this.iiiBox.UseVisualStyleBackColor = true;
            // 
            // iioBox
            // 
            this.iioBox.AutoSize = true;
            this.iioBox.Checked = true;
            this.iioBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iioBox.Location = new System.Drawing.Point(80, 116);
            this.iioBox.Name = "iioBox";
            this.iioBox.Size = new System.Drawing.Size(50, 17);
            this.iioBox.TabIndex = 14;
            this.iioBox.Text = "1 1 0";
            this.iioBox.UseVisualStyleBackColor = true;
            // 
            // ioiBox
            // 
            this.ioiBox.AutoSize = true;
            this.ioiBox.Location = new System.Drawing.Point(24, 139);
            this.ioiBox.Name = "ioiBox";
            this.ioiBox.Size = new System.Drawing.Size(50, 17);
            this.ioiBox.TabIndex = 13;
            this.ioiBox.Text = "1 0 1";
            this.ioiBox.UseVisualStyleBackColor = true;
            // 
            // iooBox
            // 
            this.iooBox.AutoSize = true;
            this.iooBox.Checked = true;
            this.iooBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iooBox.Location = new System.Drawing.Point(24, 116);
            this.iooBox.Name = "iooBox";
            this.iooBox.Size = new System.Drawing.Size(50, 17);
            this.iooBox.TabIndex = 12;
            this.iooBox.Text = "1 0 0";
            this.iooBox.UseVisualStyleBackColor = true;
            // 
            // genTab
            // 
            this.genTab.Controls.Add(this.oneDTab);
            this.genTab.Controls.Add(this.lifeTab);
            this.genTab.Controls.Add(this.TwoDGenTab);
            this.genTab.Location = new System.Drawing.Point(618, 35);
            this.genTab.Name = "genTab";
            this.genTab.SelectedIndex = 0;
            this.genTab.Size = new System.Drawing.Size(315, 580);
            this.genTab.TabIndex = 16;
            this.genTab.SelectedIndexChanged += new System.EventHandler(this.genTab_SelectedIndexChanged);
            // 
            // oneDTab
            // 
            this.oneDTab.BackColor = System.Drawing.SystemColors.Control;
            this.oneDTab.Controls.Add(this.oneDimRandButton);
            this.oneDTab.Controls.Add(this.label3);
            this.oneDTab.Controls.Add(this.label1);
            this.oneDTab.Controls.Add(this.oneDimRunButton);
            this.oneDTab.Controls.Add(this.iiiBox);
            this.oneDTab.Controls.Add(this.iioBox);
            this.oneDTab.Controls.Add(this.ioiBox);
            this.oneDTab.Controls.Add(this.iooBox);
            this.oneDTab.Controls.Add(this.oooBox);
            this.oneDTab.Controls.Add(this.oiiBox);
            this.oneDTab.Controls.Add(this.oioBox);
            this.oneDTab.Controls.Add(this.ooiBox);
            this.oneDTab.Location = new System.Drawing.Point(4, 22);
            this.oneDTab.Name = "oneDTab";
            this.oneDTab.Padding = new System.Windows.Forms.Padding(3);
            this.oneDTab.Size = new System.Drawing.Size(307, 554);
            this.oneDTab.TabIndex = 0;
            this.oneDTab.Text = "One Dimensional";
            // 
            // oneDimRandButton
            // 
            this.oneDimRandButton.Location = new System.Drawing.Point(158, 93);
            this.oneDimRandButton.Name = "oneDimRandButton";
            this.oneDimRandButton.Size = new System.Drawing.Size(120, 40);
            this.oneDimRandButton.TabIndex = 17;
            this.oneDimRandButton.Text = "Randomize Rule Set";
            this.oneDimRandButton.UseVisualStyleBackColor = true;
            this.oneDimRandButton.Click += new System.EventHandler(this.oneDimRandButton_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(295, 231);
            this.label3.TabIndex = 16;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // lifeTab
            // 
            this.lifeTab.BackColor = System.Drawing.SystemColors.Control;
            this.lifeTab.Controls.Add(this.label8);
            this.lifeTab.Controls.Add(this.liveCellSlider);
            this.lifeTab.Controls.Add(this.linkLabel1);
            this.lifeTab.Controls.Add(this.pauseBox);
            this.lifeTab.Controls.Add(this.label5);
            this.lifeTab.Controls.Add(this.twoDSpeedBar);
            this.lifeTab.Controls.Add(this.borderlessBox);
            this.lifeTab.Controls.Add(this.simModeBox);
            this.lifeTab.Controls.Add(this.label4);
            this.lifeTab.Controls.Add(this.stopButton);
            this.lifeTab.Controls.Add(this.twoDimRunButton);
            this.lifeTab.Location = new System.Drawing.Point(4, 22);
            this.lifeTab.Name = "lifeTab";
            this.lifeTab.Padding = new System.Windows.Forms.Padding(3);
            this.lifeTab.Size = new System.Drawing.Size(307, 554);
            this.lifeTab.TabIndex = 1;
            this.lifeTab.Text = "2D Life";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Initial live cell frequency:";
            // 
            // liveCellSlider
            // 
            this.liveCellSlider.LargeChange = 1;
            this.liveCellSlider.Location = new System.Drawing.Point(147, 115);
            this.liveCellSlider.Minimum = 1;
            this.liveCellSlider.Name = "liveCellSlider";
            this.liveCellSlider.Size = new System.Drawing.Size(117, 45);
            this.liveCellSlider.TabIndex = 26;
            this.liveCellSlider.TabStop = false;
            this.liveCellSlider.Value = 5;
            this.liveCellSlider.ValueChanged += new System.EventHandler(this.liveCellSlider_ValueChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(5, 533);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(99, 13);
            this.linkLabel1.TabIndex = 25;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "More Custom Rules";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pauseBox
            // 
            this.pauseBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pauseBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pauseBox.Enabled = false;
            this.pauseBox.Location = new System.Drawing.Point(6, 46);
            this.pauseBox.MaximumSize = new System.Drawing.Size(147, 23);
            this.pauseBox.MinimumSize = new System.Drawing.Size(147, 23);
            this.pauseBox.Name = "pauseBox";
            this.pauseBox.Size = new System.Drawing.Size(147, 23);
            this.pauseBox.TabIndex = 24;
            this.pauseBox.Text = "Pause";
            this.pauseBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.pauseBox.UseVisualStyleBackColor = true;
            this.pauseBox.Click += new System.EventHandler(this.pauseBox_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Speed: ";
            // 
            // twoDSpeedBar
            // 
            this.twoDSpeedBar.LargeChange = 50;
            this.twoDSpeedBar.Location = new System.Drawing.Point(64, 79);
            this.twoDSpeedBar.Maximum = 1000;
            this.twoDSpeedBar.Minimum = 500;
            this.twoDSpeedBar.Name = "twoDSpeedBar";
            this.twoDSpeedBar.Size = new System.Drawing.Size(117, 45);
            this.twoDSpeedBar.SmallChange = 20;
            this.twoDSpeedBar.TabIndex = 22;
            this.twoDSpeedBar.TabStop = false;
            this.twoDSpeedBar.Value = 900;
            this.twoDSpeedBar.ValueChanged += new System.EventHandler(this.SpeedBar_ValueChanged);
            // 
            // borderlessBox
            // 
            this.borderlessBox.AutoSize = true;
            this.borderlessBox.Checked = true;
            this.borderlessBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.borderlessBox.Location = new System.Drawing.Point(204, 86);
            this.borderlessBox.Name = "borderlessBox";
            this.borderlessBox.Size = new System.Drawing.Size(75, 17);
            this.borderlessBox.TabIndex = 21;
            this.borderlessBox.Text = "Borderless";
            this.borderlessBox.UseVisualStyleBackColor = true;
            // 
            // simModeBox
            // 
            this.simModeBox.Controls.Add(this.label7);
            this.simModeBox.Controls.Add(this.birthBox);
            this.simModeBox.Controls.Add(this.label6);
            this.simModeBox.Controls.Add(this.surviveBox);
            this.simModeBox.Controls.Add(this.customSelect);
            this.simModeBox.Controls.Add(this.assimilationSelect);
            this.simModeBox.Controls.Add(this.gameOfLifeSelect);
            this.simModeBox.Controls.Add(this.seedsSelect);
            this.simModeBox.Location = new System.Drawing.Point(6, 154);
            this.simModeBox.Name = "simModeBox";
            this.simModeBox.Size = new System.Drawing.Size(295, 148);
            this.simModeBox.TabIndex = 20;
            this.simModeBox.TabStop = false;
            this.simModeBox.Text = "Simulation Mode";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(138, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Birth:";
            // 
            // birthBox
            // 
            this.birthBox.Enabled = false;
            this.birthBox.Location = new System.Drawing.Point(175, 118);
            this.birthBox.MaxLength = 0;
            this.birthBox.Name = "birthBox";
            this.birthBox.Size = new System.Drawing.Size(63, 20);
            this.birthBox.TabIndex = 24;
            this.birthBox.Text = "3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Survive:";
            // 
            // surviveBox
            // 
            this.surviveBox.Enabled = false;
            this.surviveBox.Location = new System.Drawing.Point(64, 118);
            this.surviveBox.MaxLength = 0;
            this.surviveBox.Name = "surviveBox";
            this.surviveBox.Size = new System.Drawing.Size(63, 20);
            this.surviveBox.TabIndex = 22;
            this.surviveBox.Text = "23";
            // 
            // customSelect
            // 
            this.customSelect.AutoSize = true;
            this.customSelect.Location = new System.Drawing.Point(17, 93);
            this.customSelect.Name = "customSelect";
            this.customSelect.Size = new System.Drawing.Size(85, 17);
            this.customSelect.TabIndex = 21;
            this.customSelect.Text = "Custom Rule";
            this.customSelect.UseVisualStyleBackColor = true;
            this.customSelect.CheckedChanged += new System.EventHandler(this.TwoDSimMode_CheckedChanged);
            // 
            // assimilationSelect
            // 
            this.assimilationSelect.AutoSize = true;
            this.assimilationSelect.Location = new System.Drawing.Point(17, 70);
            this.assimilationSelect.Name = "assimilationSelect";
            this.assimilationSelect.Size = new System.Drawing.Size(118, 17);
            this.assimilationSelect.TabIndex = 20;
            this.assimilationSelect.Text = "Assimilation (Stable)";
            this.assimilationSelect.UseVisualStyleBackColor = true;
            this.assimilationSelect.CheckedChanged += new System.EventHandler(this.TwoDSimMode_CheckedChanged);
            // 
            // gameOfLifeSelect
            // 
            this.gameOfLifeSelect.AutoSize = true;
            this.gameOfLifeSelect.Checked = true;
            this.gameOfLifeSelect.Location = new System.Drawing.Point(17, 24);
            this.gameOfLifeSelect.Name = "gameOfLifeSelect";
            this.gameOfLifeSelect.Size = new System.Drawing.Size(188, 17);
            this.gameOfLifeSelect.TabIndex = 18;
            this.gameOfLifeSelect.TabStop = true;
            this.gameOfLifeSelect.Text = "Conway\'s \"Game of Life\" (Chaotic)";
            this.gameOfLifeSelect.UseVisualStyleBackColor = true;
            this.gameOfLifeSelect.CheckedChanged += new System.EventHandler(this.TwoDSimMode_CheckedChanged);
            // 
            // seedsSelect
            // 
            this.seedsSelect.AutoSize = true;
            this.seedsSelect.Location = new System.Drawing.Point(17, 47);
            this.seedsSelect.Name = "seedsSelect";
            this.seedsSelect.Size = new System.Drawing.Size(176, 17);
            this.seedsSelect.TabIndex = 19;
            this.seedsSelect.Text = "Silverman\'s \"Seeds\" (Exploding)";
            this.seedsSelect.UseVisualStyleBackColor = true;
            this.seedsSelect.CheckedChanged += new System.EventHandler(this.TwoDSimMode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 318);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 224);
            this.label4.TabIndex = 17;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(156, 46);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(145, 23);
            this.stopButton.TabIndex = 3;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // twoDimRunButton
            // 
            this.twoDimRunButton.Location = new System.Drawing.Point(6, 6);
            this.twoDimRunButton.Name = "twoDimRunButton";
            this.twoDimRunButton.Size = new System.Drawing.Size(295, 29);
            this.twoDimRunButton.TabIndex = 2;
            this.twoDimRunButton.Text = "Run Simulation";
            this.twoDimRunButton.UseVisualStyleBackColor = true;
            this.twoDimRunButton.Click += new System.EventHandler(this.twoDimRunButton_Click);
            // 
            // TwoDGenTab
            // 
            this.TwoDGenTab.BackColor = System.Drawing.SystemColors.Control;
            this.TwoDGenTab.Controls.Add(this.label9);
            this.TwoDGenTab.Controls.Add(this.genLiveCellSlider);
            this.TwoDGenTab.Controls.Add(this.linkLabel2);
            this.TwoDGenTab.Controls.Add(this.genPauseBox);
            this.TwoDGenTab.Controls.Add(this.label10);
            this.TwoDGenTab.Controls.Add(this.genSpeedBar);
            this.TwoDGenTab.Controls.Add(this.genBorderedBox);
            this.TwoDGenTab.Controls.Add(this.groupBox1);
            this.TwoDGenTab.Controls.Add(this.label13);
            this.TwoDGenTab.Controls.Add(this.genStopButton);
            this.TwoDGenTab.Controls.Add(this.genRunButton);
            this.TwoDGenTab.Location = new System.Drawing.Point(4, 22);
            this.TwoDGenTab.Name = "TwoDGenTab";
            this.TwoDGenTab.Size = new System.Drawing.Size(307, 554);
            this.TwoDGenTab.TabIndex = 2;
            this.TwoDGenTab.Text = "2D Generations";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Initial live cell frequency:";
            // 
            // genLiveCellSlider
            // 
            this.genLiveCellSlider.LargeChange = 1;
            this.genLiveCellSlider.Location = new System.Drawing.Point(147, 116);
            this.genLiveCellSlider.Minimum = 1;
            this.genLiveCellSlider.Name = "genLiveCellSlider";
            this.genLiveCellSlider.Size = new System.Drawing.Size(117, 45);
            this.genLiveCellSlider.TabIndex = 37;
            this.genLiveCellSlider.TabStop = false;
            this.genLiveCellSlider.Value = 5;
            this.genLiveCellSlider.ValueChanged += new System.EventHandler(this.liveCellSlider_ValueChanged);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(5, 534);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(99, 13);
            this.linkLabel2.TabIndex = 36;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "More Custom Rules";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // genPauseBox
            // 
            this.genPauseBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.genPauseBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.genPauseBox.Enabled = false;
            this.genPauseBox.Location = new System.Drawing.Point(6, 47);
            this.genPauseBox.MaximumSize = new System.Drawing.Size(147, 23);
            this.genPauseBox.MinimumSize = new System.Drawing.Size(147, 23);
            this.genPauseBox.Name = "genPauseBox";
            this.genPauseBox.Size = new System.Drawing.Size(147, 23);
            this.genPauseBox.TabIndex = 35;
            this.genPauseBox.Text = "Pause";
            this.genPauseBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.genPauseBox.UseVisualStyleBackColor = true;
            this.genPauseBox.Click += new System.EventHandler(this.genPauseBox_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Speed: ";
            // 
            // genSpeedBar
            // 
            this.genSpeedBar.LargeChange = 50;
            this.genSpeedBar.Location = new System.Drawing.Point(64, 80);
            this.genSpeedBar.Maximum = 1000;
            this.genSpeedBar.Minimum = 500;
            this.genSpeedBar.Name = "genSpeedBar";
            this.genSpeedBar.Size = new System.Drawing.Size(117, 45);
            this.genSpeedBar.SmallChange = 20;
            this.genSpeedBar.TabIndex = 33;
            this.genSpeedBar.TabStop = false;
            this.genSpeedBar.Value = 900;
            this.genSpeedBar.ValueChanged += new System.EventHandler(this.SpeedBar_ValueChanged);
            // 
            // genBorderedBox
            // 
            this.genBorderedBox.AutoSize = true;
            this.genBorderedBox.Checked = true;
            this.genBorderedBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.genBorderedBox.Location = new System.Drawing.Point(204, 87);
            this.genBorderedBox.Name = "genBorderedBox";
            this.genBorderedBox.Size = new System.Drawing.Size(75, 17);
            this.genBorderedBox.TabIndex = 32;
            this.genBorderedBox.Text = "Borderless";
            this.genBorderedBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.genCountBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.genBirthBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.genSurviveBox);
            this.groupBox1.Controls.Add(this.genCustomSelect);
            this.groupBox1.Controls.Add(this.brainSelect);
            this.groupBox1.Location = new System.Drawing.Point(6, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 112);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simulation Mode";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(138, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Count:";
            // 
            // genCountBox
            // 
            this.genCountBox.Enabled = false;
            this.genCountBox.Location = new System.Drawing.Point(190, 78);
            this.genCountBox.MaxLength = 0;
            this.genCountBox.Name = "genCountBox";
            this.genCountBox.Size = new System.Drawing.Size(63, 20);
            this.genCountBox.TabIndex = 26;
            this.genCountBox.Text = "3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(138, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Birth:";
            // 
            // genBirthBox
            // 
            this.genBirthBox.Enabled = false;
            this.genBirthBox.Location = new System.Drawing.Point(190, 48);
            this.genBirthBox.MaxLength = 0;
            this.genBirthBox.Name = "genBirthBox";
            this.genBirthBox.Size = new System.Drawing.Size(63, 20);
            this.genBirthBox.TabIndex = 24;
            this.genBirthBox.Text = "2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(138, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Survive:";
            // 
            // genSurviveBox
            // 
            this.genSurviveBox.Enabled = false;
            this.genSurviveBox.Location = new System.Drawing.Point(190, 19);
            this.genSurviveBox.MaxLength = 0;
            this.genSurviveBox.Name = "genSurviveBox";
            this.genSurviveBox.Size = new System.Drawing.Size(63, 20);
            this.genSurviveBox.TabIndex = 22;
            // 
            // genCustomSelect
            // 
            this.genCustomSelect.AutoSize = true;
            this.genCustomSelect.Location = new System.Drawing.Point(17, 50);
            this.genCustomSelect.Name = "genCustomSelect";
            this.genCustomSelect.Size = new System.Drawing.Size(85, 17);
            this.genCustomSelect.TabIndex = 21;
            this.genCustomSelect.Text = "Custom Rule";
            this.genCustomSelect.UseVisualStyleBackColor = true;
            this.genCustomSelect.CheckedChanged += new System.EventHandler(this.genSimMode_CheckedChanged);
            // 
            // brainSelect
            // 
            this.brainSelect.AutoSize = true;
            this.brainSelect.Checked = true;
            this.brainSelect.Location = new System.Drawing.Point(17, 24);
            this.brainSelect.Name = "brainSelect";
            this.brainSelect.Size = new System.Drawing.Size(83, 17);
            this.brainSelect.TabIndex = 18;
            this.brainSelect.TabStop = true;
            this.brainSelect.Text = "Brian\'s Brain";
            this.brainSelect.UseVisualStyleBackColor = true;
            this.brainSelect.CheckedChanged += new System.EventHandler(this.genSimMode_CheckedChanged);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(6, 281);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(295, 240);
            this.label13.TabIndex = 30;
            this.label13.Text = resources.GetString("label13.Text");
            // 
            // genStopButton
            // 
            this.genStopButton.Enabled = false;
            this.genStopButton.Location = new System.Drawing.Point(156, 47);
            this.genStopButton.Name = "genStopButton";
            this.genStopButton.Size = new System.Drawing.Size(145, 23);
            this.genStopButton.TabIndex = 29;
            this.genStopButton.Text = "Stop";
            this.genStopButton.UseVisualStyleBackColor = true;
            this.genStopButton.Click += new System.EventHandler(this.genStopButton_Click);
            // 
            // genRunButton
            // 
            this.genRunButton.Location = new System.Drawing.Point(6, 7);
            this.genRunButton.Name = "genRunButton";
            this.genRunButton.Size = new System.Drawing.Size(295, 29);
            this.genRunButton.TabIndex = 28;
            this.genRunButton.Text = "Run Simulation";
            this.genRunButton.UseVisualStyleBackColor = true;
            this.genRunButton.Click += new System.EventHandler(this.genRunButton_Click);
            // 
            // gridPanel
            // 
            this.gridPanel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gridPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gridPanel.Location = new System.Drawing.Point(9, 14);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(601, 601);
            this.gridPanel.TabIndex = 0;
            this.gridPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.gridPanel_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 622);
            this.Controls.Add(this.genTab);
            this.Controls.Add(this.gridPanel);
            this.Controls.Add(this.gridCheck);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.resolutionBar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(952, 661);
            this.MinimumSize = new System.Drawing.Size(952, 661);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cellular Automata Simulator";
            ((System.ComponentModel.ISupportInitialize)(this.resolutionBar)).EndInit();
            this.genTab.ResumeLayout(false);
            this.oneDTab.ResumeLayout(false);
            this.oneDTab.PerformLayout();
            this.lifeTab.ResumeLayout(false);
            this.lifeTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.liveCellSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoDSpeedBar)).EndInit();
            this.simModeBox.ResumeLayout(false);
            this.simModeBox.PerformLayout();
            this.TwoDGenTab.ResumeLayout(false);
            this.TwoDGenTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.genLiveCellSlider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.genSpeedBar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button oneDimRunButton;
        private System.Windows.Forms.CheckBox gridCheck;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TrackBar resolutionBar;
        private System.Windows.Forms.CheckBox oooBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ooiBox;
        private System.Windows.Forms.CheckBox oioBox;
        private System.Windows.Forms.CheckBox oiiBox;
        private System.Windows.Forms.CheckBox iiiBox;
        private System.Windows.Forms.CheckBox iioBox;
        private System.Windows.Forms.CheckBox ioiBox;
        private System.Windows.Forms.CheckBox iooBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button twoDimRunButton;
        private System.Windows.Forms.Button oneDimRandButton;
        public System.Windows.Forms.TabControl genTab;
        public System.Windows.Forms.TabPage oneDTab;
        public System.Windows.Forms.TabPage lifeTab;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.RadioButton seedsSelect;
        private System.Windows.Forms.RadioButton gameOfLifeSelect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox simModeBox;
        public CustomPanel gridPanel;
        private System.Windows.Forms.CheckBox borderlessBox;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TrackBar twoDSpeedBar;
        private System.Windows.Forms.CheckBox pauseBox;
        private System.Windows.Forms.RadioButton assimilationSelect;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox surviveBox;
        private System.Windows.Forms.RadioButton customSelect;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox birthBox;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TrackBar liveCellSlider;
        private System.Windows.Forms.TabPage TwoDGenTab;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TrackBar genLiveCellSlider;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.CheckBox genPauseBox;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TrackBar genSpeedBar;
        private System.Windows.Forms.CheckBox genBorderedBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox genCountBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox genBirthBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox genSurviveBox;
        private System.Windows.Forms.RadioButton genCustomSelect;
        private System.Windows.Forms.RadioButton brainSelect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button genStopButton;
        private System.Windows.Forms.Button genRunButton;
    }
}

