﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Linq;

namespace Cellular_Automata_Project
{
    public partial class MainForm : Form
    {
        public int resolution = 0;
        public float padding = 0.5f;
        public const float paddingDefault = 0.5f;
        OneDimension oneDim;
        TwoDimension twoDim;
        Thread mainThread;
        ThreadStart oneDimRef;
        Thread oneDimThread;
        ThreadStart gameOfLifeRef;
        Thread gameOfLifeThread;

        public static Bitmap bmp = new Bitmap(600, 600);

        static bool gol_isRunning = false;
        public static bool gol_AbortRequested = false;
        public static bool gol_Paused = false;

        public MainForm()
        {
            InitializeComponent();
            
            oneDim = new OneDimension();
            oneDim.gridPanel = gridPanel;

            twoDim = new TwoDimension();
            twoDim.gridPanel = gridPanel;

            mainThread = Thread.CurrentThread;
        }

        #region UI

        private void gridCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (gridCheck.Checked)
            {
                padding = paddingDefault;
            }
            else
            {
                padding = 0;
            }
        }

        private void resolutionBar_ValueChanged(object sender, EventArgs e)
        {
            resolution = resolutionBar.Value * 2;
        }

        private void gridPanel_Paint(object sender, PaintEventArgs e)
        {
            if (gridPanel.BorderStyle == BorderStyle.FixedSingle)
            {
                float thickness = padding;
                float halfThickness = thickness / 2;
                using (Pen p = new Pen(Color.Black, thickness))
                {
                    PointF[] points = new PointF[4];
                    points[0] = new PointF(halfThickness, halfThickness);
                    points[1] = new PointF(gridPanel.ClientSize.Width - thickness, halfThickness);
                    points[2] = new PointF(gridPanel.ClientSize.Width - thickness, gridPanel.ClientSize.Height - thickness);
                    points[3] = new PointF(halfThickness, gridPanel.ClientSize.Height - thickness);
                    e.Graphics.DrawPolygon(p, points);
                }
            }

            e.Graphics.DrawImage(bmp, new Point(0, 0));
        }

        private void SpeedBar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar rb = sender as TrackBar;
            switch (rb.Name)
            {
                case "twoDSpeedBar":
                    twoDim.delay = 1000 - twoDSpeedBar.Value;
                    genSpeedBar.Value = twoDSpeedBar.Value;
                    break;
                case "genSpeedBar":
                    twoDim.delay = 1000 - genSpeedBar.Value;
                    twoDSpeedBar.Value = genSpeedBar.Value;
                    break;
            }
        }

        private void liveCellSlider_ValueChanged(object sender, EventArgs e)
        {
            TrackBar rb = sender as TrackBar;
            switch (rb.Name)
            {
                case "liveCellSlider":
                    twoDim.initOnFreq = liveCellSlider.Value;
                    genLiveCellSlider.Value = liveCellSlider.Value;
                    break;
                case "genLiveCellSlider":
                    twoDim.initOnFreq = genLiveCellSlider.Value;
                    liveCellSlider.Value = genLiveCellSlider.Value;
                    break;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("http://psoup.math.wisc.edu/mcell/rullex_life.html");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("http://psoup.math.wisc.edu/mcell/rullex_gene.html");
        }

        private void genTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            gol_AbortRequested = true;
            TabControl tc = sender as TabControl;
            switch (tc.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    gol_AbortRequested = true;
                    pauseBox.Checked = false;
                    pauseBox.Text = "Pause";
                    pauseBox.Enabled = false;
                    stopButton.Enabled = false;
                    twoDimRunButton.Enabled = true;
                    gol_Paused = false;
                    twoDim.simMode = TwoDimension.Mode.gameOfLife;
                    gameOfLifeSelect.Checked = true;
                    break;
                case 2:
                    gol_AbortRequested = true;
                    genPauseBox.Checked = false;
                    genPauseBox.Text = "Pause";
                    genPauseBox.Enabled = false;
                    genStopButton.Enabled = false;
                    genRunButton.Enabled = true;
                    gol_Paused = false;
                    twoDim.simMode = TwoDimension.Mode.brain;
                    brainSelect.Checked = true;
                    break;
                default:
                    break;
            }
        }

        #endregion UI

        #region OneDimensional

        private void oneDimRunButton_click(object sender, EventArgs e)
        {
            // Clean up 2D tab first
            gol_AbortRequested = true;
            pauseBox.Checked = false;
            pauseBox.Text = "Pause";
            pauseBox.Enabled = false;
            stopButton.Enabled = false;
            twoDimRunButton.Enabled = true;
            gol_Paused = false;

            oneDimRunButton.Enabled = false;
            oneDimRef = new ThreadStart(StartOneDimensional);
            oneDimThread = new Thread(oneDimRef);
            oneDimThread.Start();
            oneDimThread.Join();
            oneDimRunButton.Enabled = true;
        }

        private void oneDimRandButton_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int num = rand.Next(2);
            if (num == 0) { oooBox.Checked = false; } else { oooBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { ooiBox.Checked = false; } else { ooiBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { oioBox.Checked = false; } else { oioBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { oiiBox.Checked = false; } else { oiiBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { iooBox.Checked = false; } else { iooBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { ioiBox.Checked = false; } else { ioiBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { iioBox.Checked = false; } else { iioBox.Checked = true; }
            num = rand.Next(2);
            if (num == 0) { iiiBox.Checked = false; } else { iiiBox.Checked = true; }
        }

        public void StartOneDimensional()
        {
            oneDim.resolution = resolution;
            oneDim.padding = padding;
            oneDim.ooo = oooBox.Checked;
            oneDim.ooi = ooiBox.Checked;
            oneDim.oio = oioBox.Checked;
            oneDim.oii = oiiBox.Checked;
            oneDim.ioo = iooBox.Checked;
            oneDim.ioi = ioiBox.Checked;
            oneDim.iio = iioBox.Checked;
            oneDim.iii = iiiBox.Checked;
            oneDim.RunSimulation();
            oneDimThread.Abort();
        }

        #endregion OneDimensional

        #region TwoDimensional-Life

        private void TwoDSimMode_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (!rb.Checked) { return; }
            switch (rb.Name)
            {
                case "gameOfLifeSelect":
                    surviveBox.Enabled = false;
                    birthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.gameOfLife;
                    break;
                case "seedsSelect":
                    surviveBox.Enabled = false;
                    birthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.seeds;
                    break;
                case "assimilationSelect":
                    surviveBox.Enabled = false;
                    birthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.assimilation;
                    break;
                case "customSelect":
                    if (!gol_isRunning)
                    {
                        surviveBox.Enabled = true;
                        birthBox.Enabled = true;
                    }
                    twoDim.simMode = TwoDimension.Mode.custom;
                    break;
                default:
                    surviveBox.Enabled = false;
                    birthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.gameOfLife;
                    break;
            }
            twoDim.modeChanged = true;
        }
        
        private void twoDimRunButton_Click(object sender, EventArgs e)
        {
            if (gol_isRunning) { return; }
            surviveBox.Enabled = false;
            birthBox.Enabled = false;
            pauseBox.Checked = false;
            pauseBox.Text = "Pause";
            pauseBox.Enabled = true;
            stopButton.Enabled = true;
            twoDimRunButton.Enabled = false;
            if (customSelect.Checked) { ValidateCustomRule(); }
            gol_AbortRequested = false;
            gol_isRunning = true;
            gol_Paused = false;
            gameOfLifeRef = new ThreadStart(StartTwoDimensional);
            gameOfLifeThread = new Thread(gameOfLifeRef);
            gameOfLifeThread.Start();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            if (customSelect.Checked)
            {
                surviveBox.Enabled = true;
                birthBox.Enabled = true;
            }
            gol_AbortRequested = true;
            pauseBox.Checked = false;
            pauseBox.Text = "Pause";
            pauseBox.Enabled = false;
            stopButton.Enabled = false;
            twoDimRunButton.Enabled = true;
            gol_Paused = false;
            gol_isRunning = false;
        }

        private void pauseBox_Click(object sender, EventArgs e)
        {
            if (pauseBox.Checked)
            {
                gol_Paused = true;
                pauseBox.Text = "Resume";
            }
            else
            {
                gol_Paused = false;
                pauseBox.Text = "Pause";
            }
        }

        private void StartTwoDimensional()
        {
            twoDim.stopSimulation = false;
            twoDim.resolution = resolution;
            twoDim.padding = padding;
            twoDim.borderless = borderlessBox.Checked;
            twoDim.RunSimulation();
            gol_isRunning = false;
        }

        private void ValidateCustomRule()
        {
            // Fix strings: remove non integers/duplicates and alphabetize
            string survival = Regex.Replace(surviveBox.Text, "[^0-9]", "");
            string birth = Regex.Replace(birthBox.Text, "[^0-9]", "");
            survival = string.Join("", String.Concat(survival.OrderBy(c => c)).Distinct());
            birth = string.Join("", String.Concat(birth.OrderBy(c => c)).Distinct());
            surviveBox.Text = survival;
            birthBox.Text = birth;
            twoDim.surviveRule = survival;
            twoDim.birthRule = birth;
        }

        #endregion TwoDimensional-Life

        #region TwoDimensional-Generations

        private void genSimMode_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (!rb.Checked) { return; }
            switch (rb.Name)
            {
                case "brainSelect":
                    genSurviveBox.Enabled = false;
                    genCountBox.Enabled = false;
                    genBirthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.brain;
                    break;
                case "genCustomSelect":
                    if (!gol_isRunning)
                    {
                        genSurviveBox.Enabled = true;
                        genCountBox.Enabled = true;
                        genBirthBox.Enabled = true;
                    }
                    twoDim.simMode = TwoDimension.Mode.genCustom;
                    break;
                default:
                    genSurviveBox.Enabled = false;
                    genCountBox.Enabled = false;
                    genBirthBox.Enabled = false;
                    twoDim.simMode = TwoDimension.Mode.brain;
                    break;
            }
            twoDim.modeChanged = true;
        }

        private void genRunButton_Click(object sender, EventArgs e)
        {
            if (gol_isRunning) { return; }
            genSurviveBox.Enabled = false;
            genCountBox.Enabled = false;
            genBirthBox.Enabled = false;
            genPauseBox.Checked = false;
            genPauseBox.Text = "Pause";
            genPauseBox.Enabled = true;
            genStopButton.Enabled = true;
            genRunButton.Enabled = false;
            if (genCustomSelect.Checked) { ValidateGenCustomRule(); }
            gol_AbortRequested = false;
            gol_isRunning = true;
            gol_Paused = false;
            gameOfLifeRef = new ThreadStart(StartGenTwoDimensional);
            gameOfLifeThread = new Thread(gameOfLifeRef);
            gameOfLifeThread.Start();
        }

        private void genPauseBox_Click(object sender, EventArgs e)
        {
            if (genPauseBox.Checked)
            {
                gol_Paused = true;
                genPauseBox.Text = "Resume";
            }
            else
            {
                gol_Paused = false;
                genPauseBox.Text = "Pause";
            }
        }

        private void genStopButton_Click(object sender, EventArgs e)
        {
            if (genCustomSelect.Checked)
            {
                genSurviveBox.Enabled = true;
                genCountBox.Enabled = true;
                genBirthBox.Enabled = true;
            }
            gol_AbortRequested = true;
            genPauseBox.Checked = false;
            genPauseBox.Text = "Pause";
            genPauseBox.Enabled = false;
            genStopButton.Enabled = false;
            genRunButton.Enabled = true;
            gol_Paused = false;
            gol_isRunning = false;
        }  

        private void StartGenTwoDimensional()
        {
            twoDim.stopSimulation = false;
            twoDim.resolution = resolution;
            twoDim.padding = padding;
            twoDim.borderless = genBorderedBox.Checked;
            twoDim.RunSimulation();
            gol_isRunning = false;
        }

        private void ValidateGenCustomRule()
        {
            // Fix strings: remove non integers/duplicates and alphabetize
            string survival = Regex.Replace(genSurviveBox.Text, "[^0-9]", "");
            string birth = Regex.Replace(genBirthBox.Text, "[^0-9]", "");
            string count = Regex.Replace(genCountBox.Text, "[^0-9]", "");
            survival = string.Join("", String.Concat(survival.OrderBy(c => c)).Distinct());
            birth = string.Join("", String.Concat(birth.OrderBy(c => c)).Distinct());
            genSurviveBox.Text = survival;
            genBirthBox.Text = birth;
            genCountBox.Text = count;
            if (genCountBox.Text == "") { genCountBox.Text = "3"; count = "3"; }
            twoDim.surviveRule = survival;
            twoDim.birthRule = birth;
            twoDim.countRule = Int32.Parse(count);
            if (twoDim.countRule < 3)
            {
                twoDim.countRule = 3;
                genCountBox.Text = "3";
            }
        }

        #endregion TwoDimensional-Generations
    }
}
