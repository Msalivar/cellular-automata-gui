﻿using System.Drawing;
using System.Windows.Forms;

namespace Cellular_Automata_Project
{
    class OneDimension
    {
        Pen whitePen = new Pen(Color.White, 1f);
        Pen blackPen = new Pen(Color.Black, 1f);
        Brush whiteBrush = Brushes.White;
        Brush blackBrush = Brushes.Black;
        float increment = 0;
        float yposition = 0;
        int[] prevStates = new int[9];
        int[] newStates = new int[9];
        int arraySize = 9;

        public Panel gridPanel;
        public int resolution;
        public float padding;

        public bool ooo, ooi, oio, oii, ioo, ioi, iio, iii;

        public void RunSimulation()
        {
            arraySize = 9 + resolution;
            increment = 600f / arraySize;
            yposition = 0;
            prevStates = new int[arraySize];
            newStates = new int[arraySize];

            using (Graphics g = Graphics.FromImage(MainForm.bmp))
            {
                g.Clear(Color.Black);

                // Set middle cell to state 1 for first generation
                int middleIndex = (arraySize) / 2;
                prevStates[middleIndex] = 1;

                for (int i = 0; i < arraySize; i++)
                {
                    // Draw row of cells
                    DrawCells(prevStates, yposition, increment, g);
                    gridPanel.Invalidate();

                    yposition += increment;

                    // Use rules to determine next generation of cells
                    newStates = new int[arraySize];
                    for (int j = 1; j < arraySize - 1; j++)
                    {
                        bool retState = DetermineState(prevStates[j - 1], prevStates[j], prevStates[j + 1]);
                        if (retState) { newStates[j] = 1; }
                        else { newStates[j] = 0; }
                    }
                    prevStates = newStates;
                }
            }
        }

        private void DrawCells(int[] states, float yposition, float increment, Graphics g)
        {
            float xposition = 0;
            for (int i = 0; i < arraySize; i++)
            {
                // This part calculates the 4 vertices of each square to draw
                PointF[] points = new PointF[4];
                points[0] = new PointF(xposition, yposition);
                points[1] = new PointF(xposition + increment, yposition);
                points[2] = new PointF(xposition + increment, yposition + increment);
                points[3] = new PointF(xposition, yposition + increment);
                g.FillPolygon(blackBrush, points);
                if (states[i] == 0)
                {
                    points[0].X += padding;
                    points[0].Y += padding;

                    points[1].X -= padding;
                    points[1].Y += padding;

                    points[2].X -= padding;
                    points[2].Y -= padding;

                    points[3].X += padding;
                    points[3].Y -= padding;
                    g.FillPolygon(whiteBrush, points);
                }
                xposition += increment;
            }
        }

        private bool DetermineState(int left, int mid, int right)
        {
            // Not efficient at all
            if (left == 0 && mid == 0 && right == 0) { return ooo; }
            else if (left == 0 && mid == 0 && right == 1) { return ooi; }
            else if (left == 0 && mid == 1 && right == 0) { return oio; }
            else if (left == 0 && mid == 1 && right == 1) { return oii; }
            else if (left == 1 && mid == 0 && right == 0) { return ioo; }
            else if (left == 1 && mid == 0 && right == 1) { return ioi; }
            else if (left == 1 && mid == 1 && right == 0) { return iio; }
            else if (left == 1 && mid == 1 && right == 1) { return iii; }
            else { MessageBox.Show("Error determining a state."); return false; }
        }
    }
}
