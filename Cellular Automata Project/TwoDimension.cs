﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace Cellular_Automata_Project
{
    class TwoDimension
    {
        public enum Mode
        {
            gameOfLife,
            seeds,
            assimilation,
            custom,
            brain,
            genCustom
        }

        public struct Cell
        {
            public Cell(int _x, int _y, int _state)
            {
                x = _x;
                y = _y;
                state = _state;
            }

            public int x;
            public int y;
            public int state;
        }
        
        Pen whitePen = new Pen(Color.White, 1f);
        Pen blackPen = new Pen(Color.Black, 1f);
        Brush whiteBrush = Brushes.White;
        Brush blackBrush = Brushes.Black;
        float increment = 0;
        int[,] prevStates;
        int[,] newStates;
        int arraySize = 9;
        List<Cell> changedCells = new List<Cell>();
        List<Color> colorList = new List<Color>();

        public Mode simMode = Mode.gameOfLife;
        public string surviveRule = "23";
        public string birthRule = "3";
        public int countRule = 3;
        List<int> survivalNums;
        List<int> birthNums;
        int countNum = 3;
        public int initOnFreq = 5;
        public Panel gridPanel;
        public int resolution;
        public float padding;
        public bool stopSimulation = false;
        public bool borderless = true;
        public int delay = 100;
        public bool modeChanged = true;
        
        public void RunSimulation()
        {
            arraySize = 9 + resolution;
            prevStates = new int[arraySize, arraySize];
            newStates = new int[arraySize, arraySize];
            LoadStateConditions();
            modeChanged = false;
            using (Graphics g = Graphics.FromImage(MainForm.bmp))
            {
                g.Clear(Color.Black);
                increment = 600f / arraySize;
                InitializeCells();
                DrawCells(prevStates, g);
                g.DrawImage(MainForm.bmp, new Point(0, 0));
                gridPanel.Invalidate();
                while (!stopSimulation)
                {
                    if (MainForm.gol_AbortRequested) { break; }
                    if (MainForm.gol_Paused) { continue; }
                    if (modeChanged)
                    {
                        LoadStateConditions();
                        modeChanged = false;
                    }
                    if (simMode == Mode.brain || simMode == Mode.genCustom)
                    {
                        if (!borderless) { DetermineGenStatesBordered(); }
                        else { DetermineGenStatesBorderless(); }
                    }
                    else
                    {
                        if (!borderless) { DetermineStatesBordered(); }
                        else { DetermineStatesBorderless(); }
                    }
                    UpdateCells(changedCells, g);
                    gridPanel.Invalidate();
                    Thread.Sleep(delay);
                }
            }
        }

        void InitializeCells()
        {
            if (simMode == Mode.seeds)
            {
                int index = arraySize / 2;
                Random rand = new Random();
                int numToActivate = rand.Next(7) + 2;
                for (int i = 0; i < numToActivate; i++)
                {
                    int x = rand.Next(4);
                    int y = rand.Next(4);
                    prevStates[index + x, index + y] = 1;
                }
            }
            else if(simMode == Mode.assimilation) { InitAllCellsByChance(3); }
            else { InitAllCellsByChance(initOnFreq); }
        }

        void InitAllCellsByChance(int chance)
        {
            int start = 1;
            int finish = arraySize - 1;
            if (borderless)
            {
                start = 0;
                finish = arraySize;
            }
            Random rand = new Random();
            for (int i = start; i < finish; i++)
            {
                for (int j = start; j < finish; j++)
                {
                    int roll = rand.Next(1, 10);
                    if (roll <= chance) { prevStates[i, j] = 1; }
                    else { prevStates[i, j] = 0; }
                }
            }
        }

        void DrawCells(int[,] states, Graphics g)
        {
            float xposition = 0;
            float yposition = 0;
            for (int i = 0; i < arraySize; i++)
            {
                xposition = 0;
                for (int j = 0; j < arraySize; j++)
                {
                    PointF[] points = new PointF[4];
                    points[0] = new PointF(xposition, yposition);
                    points[1] = new PointF(xposition + increment, yposition);
                    points[2] = new PointF(xposition + increment, yposition + increment);
                    points[3] = new PointF(xposition, yposition + increment);
                    using (Brush customBrush = new SolidBrush(ChooseStateColor(states[i, j])))
                    {
                        g.FillPolygon(blackBrush, points);
                        points[0].X += padding;
                        points[0].Y += padding;

                        points[1].X -= padding;
                        points[1].Y += padding;

                        points[2].X -= padding;
                        points[2].Y -= padding;

                        points[3].X += padding;
                        points[3].Y -= padding;
                        g.FillPolygon(customBrush, points);
                    }
                    xposition += increment;
                }
                yposition += increment;
            }
        }

        void UpdateCells(List<Cell> updated, Graphics g)
        {
            foreach (Cell cell in updated)
            {
                float xposition = cell.x * increment;
                float yposition = cell.y * increment;
                PointF[] points = new PointF[4];
                points[0] = new PointF(xposition, yposition);
                points[1] = new PointF(xposition + increment, yposition);
                points[2] = new PointF(xposition + increment, yposition + increment);
                points[3] = new PointF(xposition, yposition + increment);
                using (Brush customBrush = new SolidBrush(ChooseStateColor(cell.state)))
                {
                    g.FillPolygon(blackBrush, points);
                    points[0].X += padding;
                    points[0].Y += padding;

                    points[1].X -= padding;
                    points[1].Y += padding;

                    points[2].X -= padding;
                    points[2].Y -= padding;

                    points[3].X += padding;
                    points[3].Y -= padding;
                    g.FillPolygon(customBrush, points);
                }
            }
        }

        void DetermineStatesBorderless()
        {
            Array.Copy(prevStates, newStates, arraySize * arraySize);
            changedCells.Clear();
            for (int x = 0; x < arraySize; x++)
            {
                for (int y = 0; y < arraySize; y++)
                {
                    int neighbors = 0;
                    
                    // not on an edge
                    if ((x > 0 && x < arraySize - 1) && (y > 0 && y < arraySize - 1))
                    {
                        neighbors += prevStates[x - 1, y - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, y + 1];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[x + 1, y - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, y + 1];
                    }
                    // top edge only
                    else if (x == 0 && (y > 0 && y < arraySize - 1))
                    {
                        neighbors += prevStates[arraySize - 1, y - 1];
                        neighbors += prevStates[arraySize - 1, y];
                        neighbors += prevStates[arraySize - 1, y + 1];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[x + 1, y - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, y + 1];
                    }
                    // left edge only
                    else if (y == 0 && (x > 0 && x < arraySize - 1))
                    {
                        neighbors += prevStates[x - 1, arraySize - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, y + 1];
                        neighbors += prevStates[x, arraySize - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[x + 1, arraySize - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, y + 1];
                    }
                    // right edge only
                    else if (y == arraySize - 1 && (x > 0 && x < arraySize - 1))
                    {
                        neighbors += prevStates[x - 1, y - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, 0];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, 0];
                        neighbors += prevStates[x + 1, y - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, 0];
                    }
                    // bottom edge only
                    else if (x == arraySize - 1 && (y > 0 && y < arraySize - 1))
                    {
                        neighbors += prevStates[x - 1, y - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, y + 1];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[0, y - 1];
                        neighbors += prevStates[0, y];
                        neighbors += prevStates[0, y + 1];
                    }
                    // top left corner
                    else if (x == 0 && y == 0)
                    {
                        neighbors += prevStates[arraySize - 1, arraySize - 1];
                        neighbors += prevStates[arraySize - 1, y];
                        neighbors += prevStates[arraySize - 1, y + 1];
                        neighbors += prevStates[x, arraySize - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[x + 1, arraySize - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, y + 1];
                    }
                    // top right corner
                    else if (x == 0 && y == arraySize - 1)
                    {
                        neighbors += prevStates[arraySize - 1, y - 1];
                        neighbors += prevStates[arraySize - 1, y];
                        neighbors += prevStates[arraySize - 1, 0];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, 0];
                        neighbors += prevStates[x + 1, y - 1];
                        neighbors += prevStates[x + 1, y];
                        neighbors += prevStates[x + 1, 0];
                    }
                    // bottom left corner
                    else if (x == arraySize - 1 && y == 0)
                    {
                        neighbors += prevStates[x - 1, arraySize - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, y + 1];
                        neighbors += prevStates[x, arraySize - 1];
                        neighbors += prevStates[x, y + 1];
                        neighbors += prevStates[0, arraySize - 1];
                        neighbors += prevStates[0, y];
                        neighbors += prevStates[0, y + 1];
                    }
                    // bottom right corner
                    else if (x == arraySize - 1 && y == arraySize - 1)
                    {
                        neighbors += prevStates[x - 1, y - 1];
                        neighbors += prevStates[x - 1, y];
                        neighbors += prevStates[x - 1, 0];
                        neighbors += prevStates[x, y - 1];
                        neighbors += prevStates[x, 0];
                        neighbors += prevStates[0, y - 1];
                        neighbors += prevStates[0, y];
                        neighbors += prevStates[0, 0];
                    }
                    else
                    {
                        MessageBox.Show("Error getting state of cell: " + x.ToString() + ", " + y.ToString());
                    }
                    ChooseState(neighbors, x, y);
                }
            }
            Array.Copy(newStates, prevStates, arraySize * arraySize);
        }

        void DetermineStatesBordered()
        {
            Array.Copy(prevStates, newStates, arraySize * arraySize);
            changedCells.Clear();
            for (int x = 1; x < arraySize - 1; x++)
            {
                for (int y = 1; y < arraySize - 1; y++)
                {
                    int neighbors = 0;
                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            neighbors += prevStates[x + i, y + j];
                        }
                    }
                    neighbors -= prevStates[x, y];
                    ChooseState(neighbors, x, y);
                }
            }
            Array.Copy(newStates, prevStates, arraySize * arraySize);
        }

        void DetermineGenStatesBorderless()
        {
            Array.Copy(prevStates, newStates, arraySize * arraySize);
            changedCells.Clear();
            for (int x = 0; x < arraySize; x++)
            {
                for (int y = 0; y < arraySize; y++)
                {
                    int neighbors = 0;

                    // not on an edge
                    if ((x > 0 && x < arraySize - 1) && (y > 0 && y < arraySize - 1))
                    {
                        if (prevStates[x - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, y + 1] == 1) { neighbors++; }
                    }
                    // top edge only
                    else if (x == 0 && (y > 0 && y < arraySize - 1))
                    {
                        if (prevStates[arraySize - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, y] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, y + 1] == 1) { neighbors++; }
                    }
                    // left edge only
                    else if (y == 0 && (x > 0 && x < arraySize - 1))
                    {
                        if (prevStates[x - 1, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, y + 1] == 1) { neighbors++; }
                    }
                    // right edge only
                    else if (y == arraySize - 1 && (x > 0 && x < arraySize - 1))
                    {
                        if (prevStates[x - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, 0] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, 0] == 1) { neighbors++; }
                        if (prevStates[x + 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, 0] == 1) { neighbors++; }
                    }
                    // bottom edge only
                    else if (x == arraySize - 1 && (y > 0 && y < arraySize - 1))
                    {
                        if (prevStates[x - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[0, y - 1] == 1) { neighbors++; }
                        if (prevStates[0, y] == 1) { neighbors++; }
                        if (prevStates[0, y + 1] == 1) { neighbors++; }
                    }
                    // top left corner
                    else if (x == 0 && y == 0)
                    {
                        if (prevStates[arraySize - 1, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, y] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, y + 1] == 1) { neighbors++; }
                    }
                    // top right corner
                    else if (x == 0 && y == arraySize - 1)
                    {
                        if (prevStates[arraySize - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, y] == 1) { neighbors++; }
                        if (prevStates[arraySize - 1, 0] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, 0] == 1) { neighbors++; }
                        if (prevStates[x + 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x + 1, y] == 1) { neighbors++; }
                        if (prevStates[x + 1, 0] == 1) { neighbors++; }
                    }
                    // bottom left corner
                    else if (x == arraySize - 1 && y == 0)
                    {
                        if (prevStates[x - 1, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, y + 1] == 1) { neighbors++; }
                        if (prevStates[x, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[x, y + 1] == 1) { neighbors++; }
                        if (prevStates[0, arraySize - 1] == 1) { neighbors++; }
                        if (prevStates[0, y] == 1) { neighbors++; }
                        if (prevStates[0, y + 1] == 1) { neighbors++; }
                    }
                    // bottom right corner
                    else if (x == arraySize - 1 && y == arraySize - 1)
                    {
                        if (prevStates[x - 1, y - 1] == 1) { neighbors++; }
                        if (prevStates[x - 1, y] == 1) { neighbors++; }
                        if (prevStates[x - 1, 0] == 1) { neighbors++; }
                        if (prevStates[x, y - 1] == 1) { neighbors++; }
                        if (prevStates[x, 0] == 1) { neighbors++; }
                        if (prevStates[0, y - 1] == 1) { neighbors++; }
                        if (prevStates[0, y] == 1) { neighbors++; }
                        if (prevStates[0, 0] == 1) { neighbors++; }
                    }
                    else
                    {
                        MessageBox.Show("Error getting state of cell: " + x.ToString() + ", " + y.ToString());
                    }
                    ChooseState(neighbors, x, y);
                }
            }
            Array.Copy(newStates, prevStates, arraySize * arraySize);
        }

        void DetermineGenStatesBordered()
        {
            Array.Copy(prevStates, newStates, arraySize * arraySize);
            changedCells.Clear();
            for (int x = 1; x < arraySize - 1; x++)
            {
                for (int y = 1; y < arraySize - 1; y++)
                {
                    int neighbors = 0;
                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            if (prevStates[x + i, y + j] == 1) { neighbors++; }
                        }
                    }
                    neighbors -= prevStates[x, y];
                    ChooseState(neighbors, x, y);
                }
            }
            Array.Copy(newStates, prevStates, arraySize * arraySize);
        }

        void LoadStateConditions()
        {
            colorList = new List<Color>();
            survivalNums = new List<int>();
            birthNums = new List<int>();
            if (simMode == Mode.custom)
            {
                for (int i = 0; i < surviveRule.Length; i++)
                {
                    survivalNums.Add(surviveRule[i] - '0');
                }
                for (int i = 0; i < birthRule.Length; i++)
                {
                    birthNums.Add(birthRule[i] - '0');
                }
                countNum = 2;
            }
            else if (simMode == Mode.seeds)
            {
                survivalNums = new List<int>();
                birthNums = new List<int>() { 2 };
                countNum = 2;
            }
            else if (simMode == Mode.assimilation)
            {
                survivalNums = new List<int>() { 4, 5, 6, 7 };
                birthNums = new List<int>() { 3, 4, 5 };
                countNum = 2;
            }
            else if (simMode == Mode.brain)
            {
                survivalNums = new List<int>();
                birthNums = new List<int>() { 2 };
                countNum = 3;
            }
            else if (simMode == Mode.genCustom)
            {
                for (int i = 0; i < surviveRule.Length; i++)
                {
                    survivalNums.Add(surviveRule[i] - '0');
                }
                for (int i = 0; i < birthRule.Length; i++)
                {
                    birthNums.Add(birthRule[i] - '0');
                }
                countNum = countRule;
            }
            else
            {
                survivalNums = new List<int>() { 2, 3 };
                birthNums = new List<int>() { 3 };
            }
        }

        void ChooseState(int neighbors, int x, int y)
        {
            // TODO: neighbor count is wrong for brian
            // Generations type simulations
            if (simMode == Mode.brain || simMode == Mode.genCustom)
            {
                if (prevStates[x, y] == 1)
                {
                    if (survivalNums.Contains(neighbors))
                    {
                        // Cell stays the same
                        newStates[x, y] = prevStates[x, y];
                    }
                    else
                    {
                        // Cell changes state
                        newStates[x, y] = prevStates[x, y] + 1;
                        changedCells.Add(new Cell(y, x, prevStates[x, y] + 1));
                    }
                }
                else if (prevStates[x, y] == 0)
                {
                    if (birthNums.Contains(neighbors))
                    {
                        // Cell created
                        newStates[x, y] = 1;
                        changedCells.Add(new Cell(y, x, 1));
                    }
                    else
                    {
                        // Cell stays the same
                        newStates[x, y] = prevStates[x, y];
                    }
                }
                else if (prevStates[x, y] < countNum - 1)
                {
                    // Cell changes state
                    newStates[x, y] = prevStates[x, y] + 1;
                    changedCells.Add(new Cell(y, x, prevStates[x, y] + 1));
                }
                else
                {
                    // Cell dies
                    newStates[x, y] = 0;
                    changedCells.Add(new Cell(y, x, 0));
                }
            }
            // Life type simulations
            else
            {
                // survival
                if (prevStates[x, y] == 1 && survivalNums.Contains(neighbors))
                {
                    // Cell stays the same
                    newStates[x, y] = prevStates[x, y];
                }
                // birth
                else if (prevStates[x, y] == 0 && birthNums.Contains(neighbors))
                {
                    // Cell created
                    newStates[x, y] = 1;
                    changedCells.Add(new Cell(y, x, 1));
                }
                else if (prevStates[x, y] == 1)
                {
                    // Cell dies
                    newStates[x, y] = 0;
                    changedCells.Add(new Cell(y, x, 0));
                }
            }
        }

        Color ChooseStateColor(int state)
        {
            if (colorList.Count == 0)
            {
                colorList.Add(Color.White);
                colorList.Add(Color.Black);
                Random rand = new Random();
                for(int i = 2; i < countNum; i++)
                {
                    int num1 = rand.Next(50, 205);
                    int num2 = rand.Next(50, 205);
                    int num3 = rand.Next(50, 205);
                    colorList.Add(Color.FromArgb(num1, num2, num3));
                }
            }
            if (state == 2)
            {
                Console.WriteLine(2);
            }
            return colorList[state];
        }
    }
}
